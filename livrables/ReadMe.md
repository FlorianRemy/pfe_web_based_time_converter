Ce document va présenter l'oragnisation du répertoire de livrable.

Sont présents dans ce répertoire 10 dossiers.

Cahier d'analyse, cahier des charges et cahier de spécification contiennent respectivement tous ces documents. Les cahiers de spécification et d'analyse sont important pour comprendre l'architecture que j'ai donné au projet.

Le dossier documentation fourni la documentation sur l'écran à tube cathodique D14-363GY/123, qui est un écran qui peut être trouvé dans les anciens oscilloscopes analogiques de l'école.

Les dossiers manuel utilisateur, demonstrateur et developpeur contiennent ces documents. Le manuel utilisateur se présente sous la forme d'un flyer pouvant être présenté au public lors des salons. Le manuel demonstrateur regroupe à la fois les fonctions de l'administrateur et du mainteneur, car c'est la personne qui sera le plus souvent au contact du système. Enfin, le manuel développeur donne toutes les clés pour démarrer correctement les recherches pour continuer le projet.

Viennent en dernier les plannings, poster et rapport de projet qui sont des livrables pouvant être utiles pour reprendre le projet, étant donné qu'ils apportent d'autres éléments de précision.
