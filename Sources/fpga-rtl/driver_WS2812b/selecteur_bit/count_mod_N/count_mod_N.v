module count_mod_N( CLK_IN, Q, aSet, aReset );

parameter NB_BITS = 4;
parameter POSEDGE = 0;
parameter MODULO = 4'd10;
parameter SET_LEVEL = 0;
parameter RESET_LEVEL = 0;
parameter UP_DOWN = 1; // 0 down 1 up

input CLK_IN, aSet, aReset;

output [(NB_BITS - 1):0 ] Q = {NB_BITS{1'b0}};
reg [(NB_BITS - 1):0 ] Q = {NB_BITS{1'b0}};

wire _clk, _set, _reset;

assign _clk = (POSEDGE) ? ~CLK_IN : CLK_IN;
assign _set = (SET_LEVEL) ? ~aSet : aSet;
assign _reset = (RESET_LEVEL) ? ~aReset : aReset;

always @( negedge( _clk ), negedge( _set ), negedge( _reset ) )
begin

	if( _set == 0 )
		begin
			Q <= (MODULO - 1);
		end
	else
		begin
			
			if( _reset == 0 )
				begin
					Q <= {NB_BITS{1'b0}};
				end
			else
				begin
					if( UP_DOWN == 1 )
						begin
							Q <= (Q + 1) % MODULO;
						end
					else
						begin
							if( Q == 0 )
								begin
									Q <= (MODULO - 1);
								end
							else
								begin
									Q <= (Q - 1);
								end
						end
				end
		end
end

endmodule