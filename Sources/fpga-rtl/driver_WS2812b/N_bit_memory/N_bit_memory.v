module N_bit_memory( input_data, output_data, save_config );

parameter N_BIT = 24;
parameter POSEDGE = 1;

input [ ( N_BIT - 1 ) : 0 ] input_data;
input save_config;

output [ ( N_BIT - 1 ) : 0 ] output_data = {N_BIT{1'b0}};
reg [ ( N_BIT - 1 ) : 0 ] output_data = {N_BIT{1'b0}};

wire _save;

assign _save = ( POSEDGE == 1 ) ? save_config : ~save_config;

always @( posedge( _save ) )
begin
	output_data <= input_data;
end
endmodule 