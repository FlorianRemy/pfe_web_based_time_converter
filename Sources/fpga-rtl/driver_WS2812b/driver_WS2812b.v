module driver_WS2812b( data, clk_in, sync, force_reset, next_led, idle_pin, dout );

parameter CLK_IN_HZ = 50000000;

input [ 23 : 0 ] data;
input clk_in, force_reset, sync;

output next_led, idle_pin, dout;

wire clk_bit, reset, save_color, color_bit, next_bit, enable_dout;

wire [ 0 : 23 ] color_led;

wire _clk_bit;
wire _pulse_startup;
wire _force_reset;

reg _startup_state = 1'b1;

assign clk_bit = ( CLK_IN_HZ > 3200000) ? _clk_bit : clk_in ;

assign _force_reset = (_startup_state) ? 0 : force_reset;

div_freq #(4 , CLK_IN_HZ / 3200000 , CLK_IN_HZ / 6400000 ,0) GenClkBit( clk_in, _clk_bit );

gene_dout doutGenerator( clk_bit, reset, color_bit, next_bit, enable_dout, dout );

cadenceur_DEL cadenceurLeds( sync, next_led, save_color, clk_bit, enable_dout, reset, 24, _force_reset, idle_pin );

N_bit_memory #(24, 1) colorLedMem( data, color_led, save_color );

selecteur_bit selectorBit( next_bit, reset, color_bit, color_led );

div_freq #( 12, 3001, 3000, 0 ) gene_temp_startup( clk_in, _pulse_startup );

always @( posedge( _pulse_startup ) ) 
begin
	
	_startup_state <= 0;

end

endmodule 