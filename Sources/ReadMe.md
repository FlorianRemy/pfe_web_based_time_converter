Ce répertoire contient toutes les sources de mes développement pour ce projet.

Le dossier aff_first_line contient la partie dédiée à l'affichage de la date courante sur la ligne d'afficheurs 14 segments.

Les dossiers fpga-rtl et hps-c contiennent les sources actuellement programmées sur la carte DE0-nano-SoC. Dans fpga-rtl, on peut trouver les sources pour le design du FPGA et dans hps-c, on retrouve le code pour l'applicatif linux.

Enfin, TOP_level_WS2812 contient un projet fonctionnel pour un driver de DEL WS2812 dédié à la carte DE0-nano-SoC
