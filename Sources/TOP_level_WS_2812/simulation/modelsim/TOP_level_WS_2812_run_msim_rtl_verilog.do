transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812 {/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/de0_nano_soc_baseline.v}
vlog -vlog01compat -work work +incdir+/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/div_freq {/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/div_freq/div_freq.v}
vlog -vlog01compat -work work +incdir+/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b {/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/driver_WS2812b.v}
vlog -vlog01compat -work work +incdir+/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/gene_dout {/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/gene_dout/gene_dout.v}
vlog -vlog01compat -work work +incdir+/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/gene_dout/gene_next_bit {/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/gene_dout/gene_next_bit/gene_next_bit.v}
vlog -vlog01compat -work work +incdir+/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/cadenceur_DEL {/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/cadenceur_DEL/cadenceur_DEL.v}
vlog -vlog01compat -work work +incdir+/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/N_bit_memory {/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/N_bit_memory/N_bit_memory.v}
vlog -vlog01compat -work work +incdir+/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/selecteur_bit {/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/selecteur_bit/selecteur_bit.v}
vlog -vlog01compat -work work +incdir+/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/selecteur_bit/count_mod_N {/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/selecteur_bit/count_mod_N/count_mod_N.v}
vlog -vlog01compat -work work +incdir+/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/selecteur_bit/mux_N1 {/home/florian/Documents/Polytech/PFE/sources_HDL/TOP_level_WS_2812/driver_WS2812b/selecteur_bit/mux_N1/mux_N1.v}

