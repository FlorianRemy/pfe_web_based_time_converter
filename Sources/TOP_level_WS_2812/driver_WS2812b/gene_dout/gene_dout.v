module gene_dout( CLK_IN, reset, color_bit, next_bit, enable_dout, dout );

input CLK_IN;
input reset;
input color_bit;
input enable_dout;

output next_bit;
output dout;

initial _dout = 1'b0;

reg [1:0] counter;
reg _color_bit;
reg _dout;

gene_next_bit _gene_next_bit( counter, next_bit );

assign dout = enable_dout && _dout;

always @( posedge( CLK_IN ) )
begin

if( counter == 2'b00 )
	begin
		_color_bit <= color_bit;
	end

end

always @( negedge( CLK_IN ), negedge( reset ) )
begin

if( reset == 0 )
	begin
		counter <= 0;
	end
else
	begin
		if( counter >= 3 )
			begin
				counter <= 0;
			end
		else
			begin
				counter <= ( counter + 1 ) % 4;
			end
	end

casex ( counter )
	2'b00:
		_dout <= 1;
	2'b01:
		_dout <= _color_bit;
	2'b1X:
		_dout <= 0;
endcase

end
endmodule 