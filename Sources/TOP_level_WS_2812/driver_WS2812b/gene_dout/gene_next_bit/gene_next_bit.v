module gene_next_bit( counter, next_bit );

input [1:0] counter;
output next_bit;

assign next_bit = (counter == 2'b01) ? 1 : 0;

endmodule