module cadenceur_DEL ( sync, next_led, save_color, clk_bit, enable_dout, reset, nb_leds, force_reset, idle_pin );

parameter IDLE = 0;
parameter START = 1;
parameter RUN = 2;
parameter STOP = 3;

input sync, clk_bit, force_reset;
input [ 23 : 0 ] nb_leds;

output next_led, save_color, enable_dout, reset, idle_pin;

reg reset, enable_dout, save_color, next_led;

reg [ 7 : 0 ] Cq; // Compteur de cadencement de l ’ envoi d ’ une LED
reg [ 23 : 0 ] Cled; // Compteur indiquant le numero de la led courante

reg [ 1 : 0 ] EtatPresent;

assign idle_pin = ( EtatPresent == IDLE ) ? 1 : 0;

always @ ( negedge ( clk_bit ) )
begin

	Cq <= Cq +1;

	if ( force_reset == 0 ) 
		begin
			enable_dout <= 0;
			EtatPresent <= IDLE;
		end

	else 
		begin
			case ( EtatPresent )
				IDLE : 
					begin
						enable_dout <= 0;
						save_color <= 0;

						// Attente du signal SYNC
						if ( sync == 1) 
							begin
								Cq <= 0; // Reset compteur cadencement LED
								Cled <= 0; // Indice de la led courante
								EtatPresent <= START ;
							end // end if sync
					end // end IDLE

				START :
					begin
						// Envoi du reset sur les compteurs et selecteurs
						reset <= 0;
						save_color <= 1;
						if ( Cq >= 5) 
							begin // Attente pour RESET
								reset <= 1;
								save_color <= 0;
								Cq <= 0;
								Cled <= 0;
								enable_dout <= 1;

								EtatPresent <= RUN ;
							end
						else 
							begin
								// Rien
							end
					end // end START

				RUN : 
					begin
						// Remonter le RESET
						reset <= 1;

						if ( Cq >= 24 && Cq <= 72) 
							begin
								next_led <= 1;
							end
						else 
							begin
								next_led <= 0;
							end // End else Cq >=24 Cq <=72

						if ( Cq >= 93 && Cq <= 94) 
							begin
								save_color <= 1;
							end
						else 
							begin
								save_color <= 0;
							end

						if ( Cled >= nb_leds ) 
							begin // Si on a latch toutes les leds
								Cled <= 0;
								Cq <= 0;

								enable_dout <= 0; // passer le dout a 0
								EtatPresent <= STOP ;
							end
						else 
						begin
							if ( Cq >= 95) 
								begin // Si on a latch le dernier Bit
									Cq <= 0;
									Cled <= Cled + 1;

									if ( Cled >= nb_leds - 1 ) 
									begin

										Cled <= 0;
										Cq <= 0;

										enable_dout <= 0; // passer le dout a 0
										EtatPresent <= STOP;
									end
								end
							else 
								begin
									// Rien
								end
						end
					end // end RUN

				STOP : 
					begin
						enable_dout <= 0; // passer le dout a 0
						
						// Enable Dout 0 pendant 50 us
						// 60 * Clk_bit pour le reset
						if ( Cq >= 250) 
							begin
								Cq <= 0;
								Cled <= Cled + 1;

								if ( Cled >= 4) 
									begin
										Cq <=0;
										Cled <=0;
										// Repasser en IDLE
										EtatPresent <= IDLE ;
									end
							end
					end // end STOP
			endcase
		end // End else RESET
end // End always
endmodule