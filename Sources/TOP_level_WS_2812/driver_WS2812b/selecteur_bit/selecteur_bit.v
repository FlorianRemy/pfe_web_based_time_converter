module selecteur_bit( next_bit, reset, color_bit, color_del );

input reset, next_bit;
input [ 23 : 0 ] color_del;

output color_bit;

wire [ 4 : 0 ] sel;

count_mod_N #(5, 1, 24, 0, 0, 0) cnt_sel( next_bit, sel, reset, 1 );

mux_N1 #(24 , 5) mux_sel( sel, color_del, color_bit );

endmodule 