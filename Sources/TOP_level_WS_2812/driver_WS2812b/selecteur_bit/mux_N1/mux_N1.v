module mux_N1( select, d, q );

parameter TAILLE_BUS_INPUT = 24;
parameter TAILLE_BUS_SELECT = 5;

input [ ( TAILLE_BUS_SELECT - 1 ) : 0 ] select;
input [ ( TAILLE_BUS_INPUT - 1 ) : 0 ] d;
output q;

wire q;
wire [ ( TAILLE_BUS_SELECT - 1 ) : 0 ] select;
wire [ ( TAILLE_BUS_INPUT - 1 ) : 0 ] d;

assign q = d[ select % TAILLE_BUS_INPUT ];

endmodule 