module div_freq ( ClkIn , ClkOut ) ;

parameter TAILLE_BUS =4; // Counter bus size
parameter NB =11; // Counter Period value
parameter NB_TON =2; // Counter compare end of ON polarity
parameter POLARITY =1; // 1 positive 0 negative polarity

input ClkIn ;

output ClkOut ;

reg [ ( TAILLE_BUS -1) : 0 ] counter ;
reg _ClkOut ;

assign ClkOut = POLARITY == 1 ? _ClkOut : ~ _ClkOut ;

always @ ( negedge ( ClkIn ) )
begin

	// Comptage
	if ( counter >= ( NB -1) )
		begin
			counter <= 0;
		end
	else
		begin
			counter <= counter +1;
		end

	// Etat haut
	if ( counter < NB_TON )
		begin
			_ClkOut <=1;
		end

	// Etat bas
	else
		begin
			_ClkOut <=0;
		end

end
endmodule