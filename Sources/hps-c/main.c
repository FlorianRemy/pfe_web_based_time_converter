#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "hwlib.h"
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_gpio.h"
#include "hps_0.h"

#define HW_REGS_BASE ( ALT_STM_OFST )
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

int main( int argc, char *argv[] ) {

	void *virtual_base;
	int fd;
	void *h2p_lw_led_data_addr;
	void *h2p_lw_next_led_addr;
	double j = 0;
	unsigned long led_data_value;
	unsigned char intensity_value;

	int i = 0;
	unsigned long i_max = 0;

	if( argc != 3 )
	{
		printf("command usage: %s <decimal u32 value>\n", argv[0]);
		return -1;
	}

	// map the address space for the LED registers into user space so we can interact with them.
	// we'll actually map in the entire CSR span of the HPS since we want to access various registers within that span

	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		printf( "ERROR: could not open \"/dev/mem\"...\n" );
		return( 1 );
	}

	virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );

	if( virtual_base == MAP_FAILED ) {
		printf( "ERROR: mmap() failed...\n" );
		close( fd );
		return( 1 );
	}
	
	h2p_lw_led_data_addr = virtual_base + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + LED_DATA_BASE ) & ( unsigned long)( HW_REGS_MASK ) );
	h2p_lw_next_led_addr = virtual_base + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + NEXT_DEL_BASE ) & ( unsigned long)( HW_REGS_MASK ) );

	led_data_value = strtol( argv[1], NULL, 0 );
	i_max = strtol( argv[2], NULL, 0);



	/*while( j < 500 )
	{
		for( i = 0; i < i_max; i++ ) 
		{
			switch ( i )
			{
				case 0:
					led_data_value = 0;
					break;
				default:
					led_data_value = 0x0F0F0F;
					break;
			}
			
			alt_write_word( h2p_lw_led_data_addr, led_data_value );

			//usleep( 100*1000 );

			while( alt_read_word( h2p_lw_next_led_addr ) != 1 ) {}
		}

		alt_write_word( h2p_lw_led_data_addr, ((j % i_max) ? 0 : 0x0F0F0F) );

		while( alt_read_word( h2p_lw_next_led_addr ) != 1 ) {}

		j++;
	}*/
	
	alt_write_word( h2p_lw_led_data_addr, led_data_value );

	/*while( 1 )
	{
		alt_write_word( h2p_lw_led_data_addr, (intensity_value << 16) | (intensity_value << 8) | intensity_value );

		if( i == 0 )
		{
			if( intensity_value < i_max )
			{
				intensity_value = 0;
				i = 1;
			}
			else
			{
				intensity_value -= i_max;
			}
			
		}
		else
		{
			if( intensity_value > (255 - i_max) )
			{
				intensity_value = 255;
				i = 0;
			}
			else
			{
				intensity_value += i_max;
			}
		}

		//while( alt_read_word( h2p_lw_next_led_addr ) != 1 ) {}
		usleep( 4*1000 );
	}*/
	
	// clean up our memory mapping and exit
	
	if( munmap( virtual_base, HW_REGS_SPAN ) != 0 ) {
		printf( "ERROR: munmap() failed...\n" );
		close( fd );
		return( 1 );
	}

	close( fd );

	return( 0 );
}
