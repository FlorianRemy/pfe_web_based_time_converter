module increment_date( clock_in, annee_in, mois_in, jour_in, heure_in, minute_in, annee_out, mois_out, jour_out, heure_out, minute_out );

input clock_in;
input [13:0] annee_in;
input [3:0] mois_in;
input [4:0] jour_in;
input [4:0] heure_in;
input [5:0] minute_in;

output [13:0] annee_out;
output [3:0] mois_out;
output [4:0] jour_out;
output [4:0] heure_out;
output [5:0] minute_out;

reg [13:0] annee_out;
reg [3:0] mois_out;
reg [4:0] jour_out;
reg [4:0] heure_out;
reg [5:0] minute_out;

always @( posedge( clock_in ) )
begin
	if( minute_in >= 59 )
		begin
			minute_out <= 0;
			
			if( heure_in >= 23 )
				begin
					heure_out <= 0;
					
					if( jour_in >= 28 )
						begin
							case( mois_in )
								0:
									begin
										if( jour_in >= 31 )
											begin
												jour_out <= 0;
												mois_out <= mois_in + 1;
											end
										else
											begin
												jour_out <= jour_in + 1;
											end
									end
								1:
									begin
										if( (annee_in % 4) == 0 )
											begin
												if( jour_in >= 29 )
													begin
														jour_out <= 0;
														mois_out <= mois_in + 1;
													end
												else
													begin
														jour_out <= jour_in + 1;
													end
											end
										else
											begin
												jour_out <= 0;
												mois_out <= mois_in + 1;
											end
									end
								2:
									begin
										if( jour_in >= 31 )
											begin
												jour_out <= 0;
												mois_out <= mois_in + 1;
											end
										else
											begin
												jour_out <= jour_in + 1;
											end
									end
								3:
									begin
										if( jour_in >= 30 )
											begin
												jour_out <= 0;
												mois_out <= mois_in + 1;
											end
										else
											begin
												jour_out <= jour_in + 1;
											end
									end
								4:
									begin
										if( jour_in >= 31 )
											begin
												jour_out <= 0;
												mois_out <= mois_in + 1;
											end
										else
											begin
												jour_out <= jour_in + 1;
											end
									end
								5:
									begin
										if( jour_in >= 30 )
											begin
												jour_out <= 0;
												mois_out <= mois_in + 1;
											end
										else
											begin
												jour_out <= jour_in + 1;
											end
									end
								6:
									begin
										if( jour_in >= 31 )
											begin
												jour_out <= 0;
												mois_out <= mois_in + 1;
											end
										else
											begin
												jour_out <= jour_in + 1;
											end
									end
								7:
									begin
										if( jour_in >= 31 )
											begin
												jour_out <= 0;
												mois_out <= mois_in + 1;
											end
										else
											begin
												jour_out <= jour_in + 1;
											end
									end
								8:
									begin
										if( jour_in >= 30 )
											begin
												jour_out <= 0;
												mois_out <= mois_in + 1;
											end
										else
											begin
												jour_out <= jour_in + 1;
											end
									end
								9:
									begin
										if( jour_in >= 31 )
											begin
												jour_out <= 0;
												mois_out <= mois_in + 1;
											end
										else
											begin
												jour_out <= jour_in + 1;
											end
									end
								10:
									begin
										if( jour_in >= 30 )
											begin
												jour_out <= 0;
												mois_out <= mois_in + 1;
											end
										else
											begin
												jour_out <= jour_in + 1;
											end
									end
								11:
									begin
										if( jour_in >= 31 )
											begin
												jour_out <= 0;
												mois_out <= 0;
												annee_out <= annee_in + 1;
											end
										else
											begin
												jour_out <= jour_in + 1;
											end
									end
								default:
									begin
									end
							endcase
						end
					else
						begin
							jour_out <= jour_in + 1;
						end
				end
			else
				begin
					heure_out <= heure_in + 1;
				end
		end
	else
		begin
			minute_out <= minute_in + 1;
		end
end

endmodule 