module gest_modif_date( clock_in, annee_in, mois_in, jour_in, heure_in, minute_in, annee_out, mois_out, jour_out, heure_out, minute_out, save );

input clock_in;
input [13:0] annee_in;
input [3:0] mois_in;
input [4:0] jour_in;
input [4:0] heure_in;
input [5:0] minute_in;

output save;
output [13:0] annee_out;
output [3:0] mois_out;
output [4:0] jour_out;
output [4:0] heure_out;
output [5:0] minute_out;

wire _clock_seconde;
wire _clock_minute;

reg save;

div_freq #( 26, 50000000, 25000000, 0) gene_seconde( clock_in, _clock_seconde );

div_freq #( 6, 60, 30, 0 ) gene_minute( _clock_seconde, _clock_minute );

increment_date incr_date( _clock_minute, annee_in, mois_in, jour_in, heure_in, minute_in, annee_out, mois_out, jour_out, heure_out, minute_out );

always @( posedge( _clock_seconde ) )
begin
	if( minute_out != minute_in )
		begin
			save <= 1;
		end
	else
		begin
			save <= 0;
		end
end

endmodule 