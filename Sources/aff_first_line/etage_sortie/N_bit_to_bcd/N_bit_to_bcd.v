module N_bit_to_bcd( data_in, data_out );

parameter ENTRY_DATA_WIDTH = 14;
parameter NUMBER_OF_DIGIT = 4;

input [ ( ENTRY_DATA_WIDTH - 1 ) : 0] data_in;

output [ ( ( 4 * NUMBER_OF_DIGIT ) - 1 ) : 0 ] data_out;

wire [3 : 0] mille;
wire [3 : 0] cent;
wire [3 : 0] dix;
wire [3 : 0] unite;

assign mille = (data_in >= 1000) ? (( data_in % 10000) / 1000) : 0;

assign cent = (data_in >= 100) ? (((data_in % 10000) % 1000) / 100) : 0;

assign dix = (data_in >= 100) ? ((((data_in % 10000) % 1000) % 100 ) / 10) : 0;

assign unite = data_in % 10;

assign data_out = { mille, cent, dix, unite };

endmodule 