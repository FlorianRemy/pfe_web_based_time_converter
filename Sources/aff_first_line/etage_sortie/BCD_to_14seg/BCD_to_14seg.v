module BCD_to_14seg( data_in, data_out );

input [3:0]data_in;

output [13:0]data_out;
reg [13:0] data_out;

always @( data_in )
begin
	case ( data_in )
		0:
			data_out <= 14'b11111100100010;
		1:
			data_out <= 14'b01100000000000;
		2:
			data_out <= 14'b11011000010001;
		3:
			data_out <= 14'b11110000010001;
		4:
			data_out <= 14'b01100100010001;
		5:
			data_out <= 14'b10110100010001;
		6:
			data_out <= 14'b10111100010001;
		7:
			data_out <= 14'b11100100000000;
		8:
			data_out <= 14'b11111100010001;
		9:
			data_out <= 14'b11110100010001;
		default:
			data_out <= {14{1'b0}};
	endcase

end

endmodule 