module etage_sortie( clock_in, data_annee, data_mois, data_jour, data_heure, data_minute, data_out, select_out );

parameter NB_SEG_AFFICHEUR = 14;
parameter NB_DIGIT_ANNEE = 4;
parameter NB_DIGIT_MOIS = 3;
parameter NB_DIGIT_JOUR = 2;
parameter NB_DIGIT_HEURE = 2;
parameter NB_DIGIT_MINUTE = 2;
parameter NB_DIGIT = NB_DIGIT_ANNEE + NB_DIGIT_MOIS + NB_DIGIT_JOUR + NB_DIGIT_HEURE + NB_DIGIT_MINUTE;

input clock_in;
input [13:0] data_annee;
input [3:0] data_mois;
input [4:0] data_jour;
input [4:0] data_heure;
input [5:0] data_minute;

output [ (NB_SEG_AFFICHEUR - 1) :0] data_out;
output [ (NB_DIGIT - 1) :0] select_out;

reg [ (NB_SEG_AFFICHEUR * NB_DIGIT) - 1 : 0 ] _data_interne;
reg [ ((NB_DIGIT - NB_DIGIT_MOIS) * 4) - 1 : 0 ] _data_bcd_interne;


N_bit_to_bcd #( NB_SEG_AFFICHEUR, NB_DIGIT_ANNEE ) convert_annee( data_annee, _data_bcd_interne[31:16] );
N_bit_to_bcd #( NB_SEG_AFFICHEUR, NB_DIGIT_JOUR ) convert_jour( data_jour, _data_bcd_interne[39:32] );
N_bit_to_bcd #( NB_SEG_AFFICHEUR, NB_DIGIT_HEURE ) convert_heure( data_heure, _data_bcd_interne[15:8] );
N_bit_to_bcd #( NB_SEG_AFFICHEUR, NB_DIGIT_MINUTE ) convert_minute( data_minute, _data_bcd_interne[7:0] );

BCD_to_14seg digit_1( _data_bcd_interne[39:36], _data_interne[181:168] );
BCD_to_14seg digit_2( _data_bcd_interne[35:32], _data_interne[167:154] );
binary_to_month digit_345( data_mois, _data_interne[153:112] );
BCD_to_14seg digit_6( _data_bcd_interne[31:28], _data_interne[111:98] );
BCD_to_14seg digit_7( _data_bcd_interne[27:24], _data_interne[97:84] );
BCD_to_14seg digit_8( _data_bcd_interne[23:20], _data_interne[83:70] );
BCD_to_14seg digit_9( _data_bcd_interne[19:16], _data_interne[69:56] );
BCD_to_14seg digit_10( _data_bcd_interne[15:12], _data_interne[55:42] );
BCD_to_14seg digit_11( _data_bcd_interne[11:8], _data_interne[41:28] );
BCD_to_14seg digit_12( _data_bcd_interne[7:4], _data_interne[27:14] );
BCD_to_14seg digit_13( _data_bcd_interne[3:0], _data_interne[13:0] );

selecteur_afficheur sel( clock_in, _data_interne, data_out, select_out );

endmodule 