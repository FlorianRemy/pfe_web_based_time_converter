module selecteur_afficheur( clock_in, data_in, data_out, select_out );

parameter ENTRY_DATA_WIDTH = 14;
parameter NB_OF_ENTRY = 13;

input clock_in;
input [ (ENTRY_DATA_WIDTH * NB_OF_ENTRY) - 1 : 0 ] data_in;

output [ (ENTRY_DATA_WIDTH - 1) : 0 ] data_out;
output [ (NB_OF_ENTRY - 1) : 0 ] select_out;

wire clock_cadence_aff;
wire [ ( 4 - 1 ) : 0 ] index_aff;

div_freq #( 17, 153846, 153846 / 2, 0 ) gene_clock( clock_in, clock_cadence_aff );

count_mod_N #( 4, 0, 13, 0, 0, 1 ) indexer_aff( clock_cadence_aff, index_aff, 0, 0 );

mux_afficheur #( 14, 13, 4 ) mux_aff( data_in, index_aff, data_out, select_out );

endmodule 