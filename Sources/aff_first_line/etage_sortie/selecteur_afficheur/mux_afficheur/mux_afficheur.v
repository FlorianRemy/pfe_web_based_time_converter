module mux_afficheur( data_in, select_in, data_out, select_out );

parameter ENTRY_DATA_WIDTH = 4'd14;
parameter NB_OF_ENTRY = 4'd13;
parameter SELECT_IN_WIDTH = 3'd4;

input [ ( ( NB_OF_ENTRY * ENTRY_DATA_WIDTH ) - 1 ) : 0 ] data_in;
input [ ( SELECT_IN_WIDTH - 1 ) : 0 ] select_in;

output [ ( ENTRY_DATA_WIDTH - 1 ) : 0 ] data_out;
output [ ( NB_OF_ENTRY - 1 ) : 0 ] select_out;

assign data_out = data_in >> ( select_in * ENTRY_DATA_WIDTH );

assign select_out = 1 << select_in;

endmodule 