module binary_to_month( month_number, month_as_digit );

parameter A = 11101100010001;
parameter B = 11110001010100;
parameter C = 10011100000000;
parameter D = 11110001000100;
parameter E = 10011100000001;
parameter F = 10001100000001;
parameter G = 10111100010000;
parameter H = 01101100010001;
parameter I = 10010001000100;
parameter J = 01111000000000;
parameter K = 00001100101001;
parameter L = 00011100000000;
parameter M = 01101110100000;
parameter N = 01101110001000;
parameter O = 11111100000000;
parameter P = 11001100010001;
parameter Q = 11111100001000;
parameter R = 11001100011001;
parameter S = 10110110011001;
parameter T = 10000001000100;
parameter U = 01111100000000;
parameter V = 00001100100010;
parameter W = 01101100001010;
parameter X = 00000010101010;
parameter Y = 00000010100100;
parameter Z = 10010000100010;


input [ 3 : 0 ] month_number;

output [ 11 : 0 ] month_as_digit;
reg [11 : 0 ] month_as_digit;

always @( month_number )
begin
	case (month_number)
		1:
			begin
				month_as_digit <= { J, A, N };
			end
			
		2:
			begin
				month_as_digit <= { F, E, V };
			end
			
		3:
			begin
				month_as_digit <= { M, A, R };
			end
			
		4:
			begin
				month_as_digit <= { A, V, R };
			end
			
		5:
			begin
				month_as_digit <= { M, A, I };
			end
			
		6:
			begin
				month_as_digit <= { J, U, N };
			end
			
		7:
			begin
				month_as_digit <= { J, U, I };
			end
			
		8:
			begin
				month_as_digit <= { A, O, U };
			end
			
		9:
			begin
				month_as_digit <= { S, E, P };
			end
			
		10:
			begin
				month_as_digit <= { O, C, T };
			end
			
		11:
			begin
				month_as_digit <= { N, O, V };
			end
			
		12:
			begin
				month_as_digit <= { D, E, C };
			end
			
		default:
			begin
				month_as_digit <= 0;
			end
	endcase
end

endmodule 